#!/usr/bin/env bash
set -e

sudo apt-get update
sudo apt-get install -y libreadline6-dev zlib1g-dev bison flex make gdbserver

git clone https://github.com/postgres/postgres.git
cd postgres/
git checkout REL9_6_6
./configure --enable-cassert --enable-debug CFLAGS="-ggdb -O0 -fno-omit-frame-pointer"
make
sudo make install
cd ~/
mkdir pgdata
/usr/local/pgsql/bin/initdb -D pgdata -E 'UTF-8' --no-locale
#/usr/local/pgsql/bin/postgres -D pgdata >postgreslog
#/usr/local/pgsql/bin/createdb test
#echo 0|sudo tee /proc/sys/kernel/yama/ptrace_scope